# 0.7.5.6 Fix handling of broken aura type

# 0.7.5.5 Fix for everything being broken

# 0.7.5.4 Fix for broken subTarget

# 0.7.5.3 Fix for log noise and weirdness

# 0.7.5.2 Fix for everything is a skill

# 0.7.5.1 Fix for log noise

# 0.7.5

- Fix: Subtargets were not displayed with proper label.
- New: API to register external item handlers.
	- game.modules.get('mkah-pf1-item-hints').api.addHandler(callback) – parameters: actor, item, options – return: array of Hint objects to display
	- game.modules.get('mkah-pf1-item-hints').api.Hint – class that handlers need to return

# 0.7.4

- New: GM Notes hint when they're present in an item
- New: Bad target hint for present but unidentifiable change/note targets.
- New: CMB and secondary natural attack identifiers added to combat tab.

# 0.7.3.1

- Fixed: Foundry 0.7.9 compatibility

# 0.7.3

- Fix: Container item count was not shown.
- New: Item value option now is more detailed with following options.
	- Tradegoods only
	- Tradegoods and containers
	- Tradegoods, containers, and container contents
	- All
	- None
- Note: This is no longer compatible with FVTT 0.7.x due to changes needed.

# 0.7.2

- Fix: Plain number custom hints.
- Other: Compatibility verification with FVTT 0.8.x

# 0.7.1.3

- Fix: Attacks context notes being unidentified.
- Change: Different method of falling back with unknown keys which hopefully supports custom categories to a degree.

# 0.7.1.2

- Fix: Skill changes and context notes were messed up with custom skills, subskills, and specific skills.
- Fix: No Target warning tag would incorrectly always blame Changes. Now it is ambiguous about it.

# 0.7.1.1

- Fix: Context Notes were warned about lacking formula.
- Fix: Context Note categories were being ignored.
- New: Context Notes warn about lacking text.

# 0.7.1

- Fix: Missing change target categories (`health`, `defense`, `abilityChecks`, ...).
- Fix: Better (but not great) handling of custom change targets.
- New: More nontandard change label shortenings. Also client-side option to disable them.
- Changed: Incomplete warning tag is now 'No Target'
- New: Changes with no formula are warned about with 'No Formula' tag.
- Fix: Tags could sometimes fail if bad Changes were mixed in before good ones.

# 0.7.0.1

- Fix: Errors inside containers within containers.
  Cause is due to actor information not being available. This also means some item hints can not be displayed.
- Fix: Better alt sheet support with item properties.

# 0.7 Spell school icons

- New: Added iconsf or spell schools with options to select three letter key, game-icons icons, or Thassilonian runes.
- Fixed: Sometimes custom hints were not removed when cleared. Added explicit item hint deletion to ensure it happens.
- Compatibility with PF1 0.77.22 buff changes.

# 0.6.4.1

- Iconize, Changes, and Sanity check options default to enabled for now to raise awareness of their existence.
- Repository moved

# 0.6.4 Fix for @item references

# 0.6.3

- Changed: More aggressively cache or use cached rollData to reduce impact.
- Changed: Deprecated DOMSubtreeModified swapped for the more complicated MutationObserver.
- Fixed: Custom hint retrieval could cause rest of the code to fail on malformed actor.

# 0.6.2 Custom input field in advanced tab

# 0.6.1

- Fixed: Observer could not see the hints(?)
- Fixed: Excessive decimals in item values in some cases
- Fixed: Breaking on change targets missing (e.g. removed custom changes)

# 0.6 Custom hints & container hints

- New: Items can now have custom hints by including "itemHints" dictionary flag in advanced tab.
  - Multiple hints supported via semicolon (;).
- New: Containers show item count.
- New: Containers show value of their contents.

# 0.5.0.2 Hotfix for Changes being revealed without identification

# 0.5.0.1 Hotfix for error on missing context notes

- Fixed (speculatively): Error on missing changes.

# 0.5 Newness

- New: Change targets for items. (disabled by default)
- New: Favored Class Bonus selection for classes.
- New: Item value display, default only for tradegoods.
- New: Costly spell material component hint.
- Fixed: Setting text was not displayed correctly.

# 0.4.0.2 Hotfix for enhanced armor

# 0.4.0.1 Hotfix to disable debug logging

# 0.4 Icon display & Attack hints

# 0.3 PF1 Alt Sheet compatibility

# 0.2 Masterwork & Translations

# 0.1.1 Magic Item Glow

# 0.1 Initial
