import { module, itemHandlers, exItemHandlers, createHintElement, Hint, foundry07Compatibility } from './module/common.mjs';
import './module/loader.mjs';

const iconizeSettingKey = 'iconize',
	schoolAuraKey = 'iconizeAura',
	showAllValuesKey = 'allValued',
	showChangesKey = 'changes',
	sanityKey = 'sanity',
	unlangKey = 'nonstandardLang';

const cssKey = 'm-k-item-hints',
	cssPropKey = 'm-k-item-properties',
	customHintKey = 'itemHints',
	sheetId = '.pf1.sheet,.pf1alt.sheet';

const descriptors = {
	masterwork: { label: 'PF1.Masterwork', icon: 'fas fa-gem', hint: null, additionalClasses: ['masterwork'], inBuiltTag: false },
	broken: { label: 'PF1.Broken', icon: 'fas fa-heart-broken', hint: null, additionalClasses: ['broken'], inBuiltTag: false },
	unidentified: { label: 'PF1.Unidentified', icon: 'fas fa-eye-slash', hint: null, additionalClasses: ['unidentified'], inBuiltTag: false }
};

const descriptorIcons = {
	material: { icon: 'fas fa-coins', additionalClasses: ['material'], hint: null },
	value: { label: null, icon: 'fas fa-coins', additionalClasses: ['value'], hint: null, inBuiltTag: false },
	inconsistency: { label: null, icon: 'fas fa-exclamation-circle', additionalClasses: ['inconsistency'], hint: null, inBuiltTag: false }
};

const auraIconsGI = {
	abj: 'img/game-icons/magic-shield.svg',
	con: 'img/game-icons/portal.svg',
	div: 'img/game-icons/crystal-ball.svg',
	enc: 'img/game-icons/brain-tentacle.svg',
	evo: 'img/game-icons/energy-arrow.svg',
	ill: 'img/game-icons/magic-trick.svg',
	nec: 'img/game-icons/death-juice.svg',
	trs: 'img/game-icons/frog-prince.svg',
	uni: 'img/game-icons/sparkles.svg',
};

const auraIconsTh = {
	abj: 'img/thassilonian/envy.svg',
	con: 'img/thassilonian/sloth.svg',
	div: 'img/thassilonian/truth.svg',
	enc: 'img/thassilonian/lust.svg',
	evo: 'img/thassilonian/wrath.svg',
	ill: 'img/thassilonian/pride.svg',
	nec: 'img/thassilonian/gluttony.svg',
	trs: 'img/thassilonian/greed.svg',
	uni: 'img/thassilonian/diligence.svg',
};

const auraColors = {
	abj: 'green',
	con: 'blue',
	div: '??',
	enc: 'red',
	evo: 'orange',
	ill: 'violet',
	nec: 'indigo',
	trs: 'yellow',
	uni: '??',
};

function getAuraIcon(school, type) {
	let a = type === 'runes' ? auraIconsTh : auraIconsGI;
	return `modules/${module}/${a[school] ?? a.uni}`;
}

const roundTo2 = (n) => Math.floor(n * 100) / 100;
const arrayLast = (a) => a.length > 0 ? a[a.length - 1] : null;

const sizeMap = {
	fine: 0, dim: 1, tiny: 2,
	sm: 3, med: 4, lg: 5,
	huge: 6, grg: 7, col: 8
};

/**
 * @param options Options
 * @param hints
 */
function handleAttack(options, hints) {
	const item = options.item;
	if (item.data.data.attackType === 'natural') {
		if (!item.data.data.primaryAttack)
			hints.append(createHintElement(new Hint(game.i18n.localize('MKAh.Generic.Secondary'), ['natural', 'secondary'])));
	}

	if (['mcman', 'rcman'].includes(item.data.data.actionType)) {
		hints.append(createHintElement(new Hint(game.i18n.localize('PF1.CMBAbbr'), ['combat-maneuver'])));
	}
}

function mutationCallback(mutList, _observer) {
	function mutHandler(mut) {
		const node = mut.addedNodes[0];
		if (!node.classList.contains('item-summary')) return;
		if (node.getElementsByClassName(cssPropKey).length > 0) return; // already have it

		const sheet = node.closest(sheetId),
			actorId = sheet?.id.split('actor-')?.[1];

		const item = game.actors.get(actorId)?.items.get(mut.target.dataset.itemId);
		if (!item) return;

		const props = Object.keys(item.data.data.properties ?? {}).filter(k => item.data.data.properties[k]);
		if (props.length) {
			const pEl = document.createElement('div');
			pEl.classList.add('item-properties', cssKey, cssPropKey);
			pEl.append(...props.map(key => createHintElement(new Hint(CONFIG.PF1.weaponProperties[key], ['property'], { inBuiltTag: true }))));
			node.append(pEl);
		}
	}

	Array.from(mutList).filter(m => m.addedNodes.length).forEach(mutHandler);
};

const mutationConfig = { subtree: false, childList: true, attributes: false };
const observer = new MutationObserver(mutationCallback);

/**
 * @param opts Options
 * @param hints
 */
function handleValue(opts, hints, inContainer) {
	if (opts.values === 'none') return;
	const item = opts.item;
	const quantity = item.data.data.quantity;
	if (!(quantity > 0)) return;

	const isContainer = item.type === 'container',
		isTradegoods = item.data.data.subType === 'tradeGoods';

	let showValue = false;
	switch (opts.values) {
		case 'all':
			showValue = true;
		case 'expanded':
			if (inContainer) showValue = true;
		case 'containers':
			if (isContainer) showValue = true;
		case 'tradegoods':
			if (isTradegoods) showValue = true;
			break;
		case 'none':
			return;
	}

	if (!showValue) return;

	const tCost = duplicate(descriptorIcons.value),
		gpTotal = item.getValue({ sellValue: 1 }),
		gpSelf = item.getValue({ sellValue: 1, recursive: false }),
		gpSingle = quantity > 1 ? (gpSelf / quantity) : gpSelf;

	const currency = game.i18n.localize('PF1.CurrencyGP').toLowerCase();
	const gpLabel = `${roundTo2(gpTotal)} ${currency}`;
	tCost.label = gpLabel;

	if (isContainer) {
		const trueSelf = gpSelf - item.getTotalCurrency();
		tCost.hint = `${roundTo2(trueSelf)} ${currency} (${game.i18n.localize('PF1.ItemContainerContents')}: ${roundTo2(gpTotal - trueSelf)} ${currency})`;
	} else
		tCost.hint = quantity > 1 ? `${roundTo2(gpSingle)} × ${quantity} = ${roundTo2(gpTotal)} ${currency}` : gpLabel;

	hints.append(createHintElement(tCost, opts.iconize));
}

function handleContainer(opts, hints) {
	const item = opts.item,
		count = foundry07Compatibility ? item.items.entries.length : item.items.contents.length,
		subItems = item.items.find(i => i.type === 'container'),
		coins = item.data.data.currency,
		haveCoins = coins.pp > 0 || coins.gp > 0 || coins.sp > 0 || coins.cp > 0,
		more = subItems || haveCoins;

	const iText = game.i18n.localize('PF1.Items'),
		label = `${count}${more ? '+' : ''} ${iText}`;

	const h = [`${count} ${iText}`];
	if (subItems) h.push(game.i18n.localize('PF1.InventoryContainers'));
	if (haveCoins) h.push(game.i18n.localize('PF1.Currency'));

	hints.append(createHintElement(new Hint(label, ['item-count'], { hint: h.join(' + ') })));
}

/**
 * @param opts Options
 * @param hints
 */
function handleItem(opts, hints) {
	const item = opts.item;

	if (opts.sanity && opts.showSecrets && opts.casterLevel > 0 && opts.enhancement > 0) {
		const cldiff = opts.casterLevel - (opts.enhancement * 3);
		if (cldiff < 0) { // higher CL is possible, lower is odd
			const underLevel = duplicate(descriptorIcons.inconsistency);
			underLevel.hint = game.i18n.localize('MKAh.Sanity.InsufficientCL').format(cldiff);
			hints.append(createHintElement(underLevel, true));
		}
	}

	if (item.data.data.broken) hints.append(createHintElement(descriptors.broken, opts.iconize));

	if (item.data.data.masterwork && !(opts.enhancement > 0 && opts.showSecrets))
		hints.append(createHintElement(descriptors.masterwork, opts.iconize));

	const sizeString = opts.isWeapon ? sizeMap[item.data.data.weaponData.size] : item.data.data.size;
	const size = Number.isFinite(sizeString) ? sizeString : sizeMap[sizeString];
	if (sizeString && opts.actor?.data.data.size !== size) {
		const sizeLabel = CONFIG.PF1.actorSizes[Object.keys(CONFIG.PF1.actorSizes)[size]];
		hints.append(createHintElement(new Hint(sizeLabel.capitalize(), ['size'])));
	}

	if (opts.showSecrets) {
		if (!opts.identified) hints.append(createHintElement(descriptors.unidentified, opts.iconize));

		if (opts.enhancement > 0) hints.append(createHintElement(new Hint(`+${opts.enhancement}`, ['enhancement'], { hint: game.i18n.localize('MKAh.Item.Enhancement') })));

		if (opts.casterLevel > 0) {
			const auraKey = item.data.data.aura.school,
				aura = item.data.data.aura.custom ? auraKey : CONFIG.PF1.spellSchools[auraKey];

			const hint = new Hint(`${aura} (${opts.casterLevel})`, ['aura']);
			const auraD = game.settings.get(module, schoolAuraKey);
			if (opts.iconize) {
				if (auraD === 'letters') {
					hint.hint = hint.label;
					hint.label = auraKey.length <= 3 ? game.i18n.localize(`MKAh.School.${auraKey ?? 'Unknown'}`) : game.i18n.localize('MKAh.School.odd');
				} else {
					hint.image = getAuraIcon(auraKey, auraD);
				}
			}
			hints.append(createHintElement(hint, auraD !== 'letters' ? opts.iconize : false));

			// Add glowing label (faint, moderate, strong)
			const glowLevel = Math.max(1, Math.min(3, Math.ceil((opts.casterLevel + 1) / 6)));
			opts.element.querySelector('.item-name h4').classList.add(`enhancement-${glowLevel}`);
		}
	}

	// Expanded view hints
	if (opts.isWeapon) observer.observe(opts.element, mutationConfig);
}

/**
 * @param o Options
 * @param hints hints element
 */
function handleSpell(o, hints) {
	const material = o.item.data.data.materials.value;

	const nEl = arrayLast(o.element.getElementsByClassName('spell-components'));
	// Materials use special hint
	if (material && o.item.data.data.materials.gpValue > 0 && nEl) {
		const mCost = duplicate(descriptorIcons.material);
		mCost.hint = `${game.i18n.localize('PF1.SpellComponentMaterial')}:\n${material}`;
		const mhints = document.createElement('itemhints');
		mhints.classList.add(cssKey);
		mhints.append(createHintElement(mCost, true));
		nEl.append(mhints);
	}

	const subschool = o.item.data.data.subschool,
		keywords = o.item.data.data.types;
	if (subschool?.length > 0 || keywords?.length > 0) {
		const school = arrayLast(o.element.getElementsByClassName('spell-school'));
		const sdeets = [];
		if (subschool?.length > 0) sdeets.push(`${game.i18n.localize('PF1.SubSchool')}: ${subschool}`);
		if (keywords?.length > 0) sdeets.push(`${game.i18n.localize('PF1.TypePlural')}: ${keywords}`);
		school.title = sdeets.join('\n');
	}
}

// Feats and spells
/**
 * @param o Options
 * @param hints
 */
function handleSaves(o, hints) {
	const save = o.item.data.data.save.description;
	if (save?.length && !(/harmless/i.test(save)))
		hints.append(createHintElement(new Hint(save, ['save'])));
}

	// Alter default translations to be shorter
	const coreLang = {
		bab: 'PF1.BABAbbr',
		flySpeed: 'PF1.SpeedFly_Short',
		burrowSpeed: 'PF1.SpeedBurrow_Short',
		landSpeed: 'PF1.SpeedLand_Short',
		climbSpeed: 'PF1.SpeedClimb_Short',
		swimSpeed: 'PF1.SpeedSwim_Short',
		wdamage: 'PF1.ItemTypeWeapon',
		cmb: 'PF1.CMBAbbr',
		cmd: 'PF1.CMDAbbr',
		allSpeeds: 'PF1.All',
		attack: 'PF1.All',
		mattack: 'PF1.Melee',
		rattack: 'PF1.Ranged',
		damage: 'PF1.All',
		allSavingThrows: 'PF1.All',
		mhp: 'PF1.HPShort',
		str: 'PF1.AbilityShortStr',
		dex: 'PF1.AbilityShortDex',
		con: 'PF1.AbilityShortCon',
		int: 'PF1.AbilityShortInt',
		wis: 'PF1.AbilityShortWis',
		cha: 'PF1.AbilityShortCha',
	};
	// Add extensions
	const moduleLangExtensions = {
		nac: 'MKAh.Short.NaturalAC',
		tac: 'MKAh.Short.TouchAC',
		mDexA: 'MKAh.Short.ArmorMaxDex',
		mDexS: 'MKAh.Short.ShieldMaxDex',
		ffac: 'MKAh.Short.FlatfootedAC',
		ffcmd: 'MKAh.Short.FlatfootedCMD',
		spellResist: 'MKAh.Short.SpellResistance',
		allChecks: 'MKAh.Short.AllChecks',
		// Ability score stuff
		strSkills: 'MKAh.Short.StrSkills',
		dexSkills: 'MKAh.Short.DexSkills',
		conSkills: 'MKAh.Short.ConSkills',
		intSkills: 'MKAh.Short.IntSkills',
		wisSkills: 'MKAh.Short.WisSkills',
		chaSkills: 'MKAh.Short.ChaSkills',
		strChecks: 'MKAh.Short.StrChecks',
		dexChecks: 'MKAh.Short.DexChecks',
		conChecks: 'MKAh.Short.ConChecks',
		intChecks: 'MKAh.Short.IntChecks',
		wisChecks: 'MKAh.Short.WisChecks',
		chaChecks: 'MKAh.Short.ChaChecks',
		init: 'MKAh.Short.Initiative',
		ref: 'MKAh.Short.Reflex',
		fort: 'MKAh.Short.Fortitude',
		will: 'MKAh.Short.Will'
	};

const disableUnlang = () => game.i18n.lang !== 'en' && !game.settings.get(module, unlangKey);

// Combine
let simplifications = {};
Hooks.once('init', () => simplifications = Object.assign({}, coreLang, disableUnlang() ? {} : moduleLangExtensions));

class UniformObject {
	target;
	isChange;
	formula;
	change;
	actor;
	constructor(ch, isChange, actor) {
		this.target = ch.subTarget;
		this.isChange = isChange;
		this.formula = ch.formula;
		this.change = ch;
		this.actor = actor;
	}

	get source() { return (this.isChange ? CONFIG.PF1.buffTargets : CONFIG.PF1.contextNoteTargets)[this.target]; }
	get isSkill() { return this.target.startsWith('skill.'); }
	get key() { return this.source?.category ?? (this.isSkill ? 'skill' : null); }
	get label() {
		if (this.isSkill) {
			this.target.match(/^skill\.(.*)/);
			return this.actor.getSkillInfo(RegExp.$1).name;
		}
		if (this.target in simplifications) return game.i18n.localize(simplifications[this.target]);
		return (this.isChange ? CONFIG.PF1.buffTargets : CONFIG.PF1.contextNoteTargets)[this.target]?.label ?? this.target;
	}
}

// Changes
/**
 * @param o Options
 * @param hints
 */
function handleChanges(o, hints) {
	if (!o.changes) return;
	if (!o.identified) return;
	if (!(o.item.data.data.changes?.length > 0 || o.item.data.data.contextNotes?.length > 0)) return;

	const commonTargets = {}, problems = {};

	const createUniformObject = (obj, ch, isChange, item) => {
		try {
			const cto = new UniformObject(ch, isChange, o.actor);

			if (!(cto.target?.length > 0)) {
				if (!('noTarget' in problems)) {
					console.warn('ITEM HINTS | Missing Target | Item:', item.name, '; Target:', ch, item);
					problems['noTarget'] = {
						label: game.i18n.localize('MKAh.Sanity.Change.TargetlessLabel'),
						hint: game.i18n.localize('MKAh.Sanity.Change.TargetlessHint')
					};
				}
				return obj;
			}

			if (cto.label === ch.subTarget) {
				if (!('badTarget' in problems)) {
					console.warn('ITEM HINTS | Bad Target |', item.name, ch, item);
					problems['badTarget'] = {
						label: game.i18n.localize('MKAh.Sanity.Change.BadTargetLabel'),
						hint: game.i18n.localize('MKAh.Sanity.Change.BadTargetHint')
					};
				}
				return obj;
			}
			if (isChange && !(ch.formula?.length > 0)) {
				if (!('noFormula' in problems)) {
					// Change specific
					console.warn('ITEM HINTS | Missing Formula | Item:', item.name, '; Change:', ch, item);
					problems['noFormula'] = {
						label: game.i18n.localize('MKAh.Sanity.Change.FormulalessLabel'),
						hint: game.i18n.localize('MKAh.Sanity.Change.FormulalessHint')
					}
				}
				return obj;
			}
			if (!isChange && !(ch.text?.trim().length > 0)) {
				if (!('noText' in problems)) {
					// Context note specific
					console.warn('ITEM HINTS | Missing Text |', item.name, ch, item);
					problems['noText'] = {
						label: game.i18n.localize('MKAh.Sanity.Change.TextlessLabel'),
						hint: game.i18n.localize('MKAh.Sanity.Change.TextlessHint')
					}
				}
				return obj;
			}

			const key = cto.key;
			if (key != null) {
				if (!(key in obj)) obj[key] = {};
				if (!(cto.target in obj[key])) {
					obj[key][cto.target] = cto;
				}
				if (!(key in commonTargets)) commonTargets[key] = {};
				if (!(cto.target in commonTargets[key])) commonTargets[key][cto.target] = { label: cto.label, data: cto };

				return obj;
			}
		}
		catch (err) {
			console.error("ITEM HINTS | Error:", err, ch, item);
			return obj;
		}
	}

	const ctargets = o.item.data.data.changes?.reduce((obj, ch) => createUniformObject(obj, ch, true, o.item), {}) ?? {};
	const ntargets = o.item.data.data.contextNotes?.reduce((obj, ch) => createUniformObject(obj, ch, false, o.item), {}) ?? {};

	const changeLabel = (k) => {
		switch (k) {
			case 'ability': case 'abilityChecks': return '±Ability';
			case 'ac': return '±AC';  // outdated?
			case 'cmd': return '±CMD'; case 'attack':
			case 'attack': case 'attacks': return '±ATK';
			case 'damage': return '±DMG';
			case 'savingThrows': return '±Save';
			case 'skill': case 'skills': return '±Skill';
			case 'speed': return '±Speed';
			case 'spell': return '±Spell';
			case 'misc': return '±Misc';
			case 'health': return '±Health';
			case 'defense': return '±Defense';
			default:
				try {
					const t = isChange ? CONFIG.PF1.buffTargets : CONFIG.PF1.contextNoteTargets;
					return '~' + t[k].category.capitalize();
				}
				catch (err) {
					console.warn("Item Hints | Unrecognized key:", k, "in", o.item.name, err);
					return 'UNKNOWN';
				}
		}
	}

	const makeChangeHint = (key) => {
		const formatLabel = (key) => {
			const ct = commonTargets[key];
			const b = Object.keys(ct).map(x => ct[x].label);
			const lb = changeLabel(key);
			return b.length > 0 ? `${lb}:${b.join(',')}` : lb;
		};

		let buff = false, contextual = false, both = false;

		const formatHints = (key) => {
			const cC = [];
			const cN = [];
			Object.keys(commonTargets[key])
				.forEach(x => {
					if (key in ctargets && x in ctargets[key])
						cC.push(ctargets[key][x]);
					if (key in ntargets && x in ntargets[key])
						cN.push(ntargets[key][x]);
				});
			const all = [...cC, ...cN].map(x => {
				const output = [];
				try {
					output.push(x.isChange ? 'Change' : 'Contextual');
					output.push(': ');
					output.push(x.label);
					if (x.isChange) {
						output.push(' = ~');
						output.push(x.change.value ?? x.change.formula);
					}
				}
				catch (err) {
					console.error(err, x);
				}
				return output.join('');
			});

			buff = cC.length > 0;
			contextual = cN.length > 0;
			both = buff && contextual;

			return all.join('\n');
		};

		const hints = formatHints(key);

		const css = ['change'];
		if (contextual) css.push('contextual');
		if (both) css.push('mixed');

		return new Hint(formatLabel(key), css, { hint: hints });
	}

	Object.keys(commonTargets)
		.forEach(ch => hints.append(createHintElement(makeChangeHint(ch))));

	Object.values(problems).forEach(problem => hints.append(createHintElement(new Hint(problem.label, ['change', 'error'], { hint: problem.hint }))));
}

// Change Flags
/**
 * @param opt Options
 * @param hints
 */
function handleChangeFlags(opt, hints) {
	if (!opt.changes) return;
	if (!opt.identified) return;
	const flags = opt.item.data.data.changeFlags;
	if (!flags) return;
	if (flags.heavyArmorFullSpeed) hints.append(createHintElement(new Hint('+ArmorSpd (Hvy)', ['change'])));
	if (flags.mediumArmorFullSpeed) hints.append(createHintElement(new Hint('+ArmorSpd (Med)', ['change'])));
	if (flags.loseDexToAC) hints.append(createHintElement(new Hint(`-${game.i18n.localize('MKAh.Short.DexterityToAC')}`, ['change', 'penalty'])));
	if (flags.noEncumbrance) hints.append(createHintElement(new Hint(`-${game.i18n.localize('PF1.Encumbrance')}`, ['change'])));
}

// Class
/**
 * @param o Options
 * @param hints
 */
function handleClass(o, hints) {
	const hp = o.item.data.data.fc.hp.value,
		skill = o.item.data.data.fc.skill.value,
		alt = o.item.data.data.fc.alt.value;
	if (hp > 0) hints.append(createHintElement(new Hint(`${game.i18n.localize('PF1.FavouredClassBonus.HP')} ×${hp}`, ['fcb', 'hp'])));
	if (skill > 0) hints.append(createHintElement(new Hint(`${game.i18n.localize('PF1.FavouredClassBonus.Skill')} ×${skill}`, ['fcb', 'skill'])));
	if (alt > 0) hints.append(createHintElement(new Hint(`${game.i18n.localize('PF1.FavouredClassBonus.Alt')} ×${alt}`, ['fcb', 'alt'])));

	if (o.sanity) {
		const total = hp + skill + alt;
		if (total > 0 && o.item.data.data.level !== total) {
			const shint = duplicate(descriptorIcons.inconsistency);
			shint.hint = game.i18n.localize('MKAh.Sanity.FCBInequal').format(total - o.item.data.data.level);
			shint.additionalClasses.push('fcb');
			hints.append(createHintElement(shint, true));
		}
	}
}

const getRollData = (o) => o.item.getRollData({ forceRefresh: false });

function handleCustomHints(o, hints) {
	try {
		if (o.actor?.__modItemHintsDisableCustom) return;
		let customHints = o.item.getItemDictionaryFlag(customHintKey);
		if (customHints === undefined) return;
		if (!(customHints instanceof String)) customHints = customHints.toString();
		for (let h of customHints.split(';')) {
			let parts = h.trim().split('|');
			hints.append(createHintElement(new Hint(TextEditor.enrichHTML(parts.shift().trim(), { rollData: getRollData(o) }), ['custom'], { hint: parts.join('|').trim() || null }), false));
		}
	}
	catch (err) {
		console.warn(`ITEM HINTS | Custom | ${o.actor?.name} [${o.actor.id}] | ${o.item.name}:`, o.item);
		console.error(err);
		if (actor) {
			console.debug('ITEM HINTS | Custom | Error detected; Temporarily disabling for actor:', o.actor.name);
			o.actor.__modItemHintsDisableCustom = true; // Not meant to be stored in database
		}
	}
}

function injectHints(jq, actor, items, isContainer) {
	const iEls = jq.find('.item-list .item');

	if (CONFIG.debug.modItemHints && iEls.length === 0)
		console.warn('ITEM HINTS | Could not find item elements on actor:', actor.name, actor, items);

	if (CONFIG.debug.modItemHints) console.log('ITEM HINTS |', actor.name, actor, items, iEls);

	for (let iEl of iEls) {
		const id = iEl.dataset.itemId;
		if (!id) continue; // Required by some custom sheets
		const item = items.get(id);

		if (CONFIG.debug.modItemHints) console.log(iEl, item);

		const nEl = arrayLast(iEl.getElementsByClassName('item-name'));
		if (!nEl) continue;
		const hints = document.createElement('itemhints');
		hints.classList.add(cssKey);

		const opts = {
			element: iEl,
			actor,
			item,
			iconize: game.settings.get(module, iconizeSettingKey),
			sanity: game.settings.get(module, sanityKey),
			changes: game.settings.get(module, showChangesKey),
			values: game.settings.get(module, showAllValuesKey),
			isWeapon: item.type === 'weapon',
			identified: item.data.data.identified ?? true,
			enhancement: item.data.data.enh ?? item.data.data.armor?.enh,
			casterLevel: item.data.data.cl,
			showSecrets: (item.data.data.identified ?? true) || game.user.isGM,
			rollData: null,
		};

		try {
			handleCustomHints(opts, hints);

			switch (item.type) {
				case 'feat':
					handleSaves(opts, hints);
					handleChanges(opts, hints);
					handleChangeFlags(opts, hints);
					break;
				case 'spell':
					handleSaves(opts, hints);
					handleSpell(opts, hints);
					break;
				case 'consumable':
				case 'equipment':
				case 'loot':
				case 'weapon':
					handleItem(opts, hints);
					handleChanges(opts, hints);
					handleChangeFlags(opts, hints);
					break;
				case 'attack':
					handleAttack(opts, hints);
					handleItem(opts, hints);
					break;
				case 'buff':
					handleChanges(opts, hints);
					handleChangeFlags(opts, hints);
					break;
				case 'class':
					handleClass(opts, hints);
					handleChanges(opts, hints);
					handleChangeFlags(opts, hints);
					break;
				//case 'race':
				case 'container':
					handleContainer(opts, hints);
					break;
				default:
					// NOP
					break;
			}
			handleValue(opts, hints, isContainer);

			itemHandlers.forEach(handler => handler(opts, hints));

			let handlerErrors = 0;
			exItemHandlers.forEach(handler => {
				try {
					const exHints = handler(actor, item, opts);
					for (let xh of exHints) {
						if (xh instanceof Hint) hints.append(makeHint(xh, opts.iconize));
						else console.error("ITEM HINTS | Bad return value", xh, "from external handler:", handler, "for", item.name, item);
					}
				}
				catch (err) {
					console.error("ITEM HINTS | Error in external handler:", err, "REMOVED");
					handlerErrors++;
					handler.__ih_needsDelete = true;
				}
			});
			if (handlerErrors) {
				for (let i = 0; i < exItemHandlers.length; i++) {
					if (exItemHandlers[i].__ih_needsDelete)
						exItemHandlers.splice(i, 1);
				}
			}
		}
		catch (err) {
			console.warn('ITEM HINTS | FAILURE | ID:', id, 'ITEM:', item, 'ELEMENT:', iEl);
			console.error(err);
		}

		// TODO: Strip unwanted hints based on... something?

		nEl.lastElementChild.append(hints);
	}
}

function sheetInjectionProxy(sheet, jq, options) {
	if (CONFIG.debug.modItemHints) console.log('ITEM HINTS | sheetInjectionProxy', arguments);

	injectHints(
		jq,
		sheet.actor,
		sheet.item ? sheet.item.items : sheet.actor.items, // container vs actor
		sheet.item ? true : false,
	);
}

function renderItemSheetCustomHintEditor(sheet, jq, _options) {
	const item = sheet.item, actor = sheet.actor;
	try {
		if (item.__modItemHintsDisableCustom) return;

		const oldHints = item.getItemDictionaryFlag(customHintKey) ?? '';

		const input = $('<input type="text" data-dtype="String" />').addClass('m-k-custom-hints');

		const hints = document.createElement('itemhints');
		hints.classList.add(cssKey);
		handleCustomHints({ item, actor }, hints);

		jq.find('.tab[data-tab="advanced"] > *').first()
			.append($('<div/>').addClass('m-k-item-hints')
				.append($('<h3/>').addClass('form-header').text('Custom Hints'))
				.append($('<div/>').addClass('help-text')
					.append($('<i/>').addClass('fas fa-info-circle'))
					.append(document.createTextNode(" Custom item hints can be used to show changing information about an item. Semicolon (;) can be used to separate multiple hints. Vertical bar (|) can be used to separate text from hover text. Roll data is available for inline rolls."))
				)
				.append(input)
				.append($('<h4/>').css({ 'font-weight': 'bold' }).text('Example Output'))
				.append(hints));

		async function _onCustomHintChange(ev) {
			const newValue = ev.target.value.trim();
			return newValue?.length > 0 ? item.setItemDictionaryFlag(customHintKey, newValue) : item.removeItemDictionaryFlag(customHintKey);
		}

		input.val(oldHints);

		input.on('change', _onCustomHintChange.bind());
	}
	catch (err) {
		console.warn(`ITEM HINTS | Editor | ${item.name}:`, item);
		console.debug('ITEM HINTS | Editor | Error detected; Temporarily disabling for item:', item.name);
		console.error(err);
		sheet.item.__modItemHintsDisableCustom = true; // Not meant to be stored in database
	}
}

Hooks.once('init', () => {
	game.settings.register(module, iconizeSettingKey, {
		name: 'MKAh.ItemHint.IconizeLabel',
		hint: 'MKAh.ItemHint.IconizeHint',
		type: Boolean,
		default: true,
		config: true,
		scope: 'client',
	});

	game.settings.register(module, schoolAuraKey, {
		name: 'MKAh.ItemHint.IconizeSchoolLabel',
		hint: 'MKAh.ItemHint.IconizeSchoolHint',
		type: String,
		choices: {
			'letters': 'Keys',
			'custom': 'Game Icons',
			'runes': 'Thassilonian'
		},
		default: 'letters',
		config: true,
		scope: 'client',
	});

	game.settings.register(module, showChangesKey, {
		name: 'MKAh.ItemHint.ChangeLabel',
		hint: 'MKAh.ItemHint.ChangeHint',
		type: Boolean,
		default: true,
		config: true,
		scope: 'client',
	});

	game.settings.register(module, showAllValuesKey, {
		name: 'MKAh.ItemHint.ValueLabel',
		hint: 'MKAh.ItemHint.ValueHint',
		type: String,
		choices: {
			tradegoods: 'MKAh.ItemHint.Value.TradegoodsOnly',
			containers: 'MKAh.ItemHint.Value.TradegoodsAndContainers',
			expanded: 'MKAh.ItemHint.Value.Expanded',
			all: 'MKAh.ItemHint.Value.All',
			none: 'MKAh.ItemHint.Value.None',
		},
		default: 'expanded',
		config: true,
		scope: 'client',
	});

	game.settings.register(module, sanityKey, {
		name: 'MKAh.ItemHint.SanityLabel',
		hint: 'MKAh.ItemHint.SanityHint',
		type: Boolean,
		default: true,
		config: true,
		scope: 'client',
	});

	game.settings.register(module, unlangKey, {
		name: 'MKAh.ItemHint.LangLabel',
		hint: 'MKAh.ItemHint.LangHint',
		type: Boolean,
		default: true,
		config: true,
		scope: 'client',
	});

	Hooks.on('renderActorSheetPF', sheetInjectionProxy);
	Hooks.on('renderItemSheet', sheetInjectionProxy);
	Hooks.on('renderItemSheet', renderItemSheetCustomHintEditor);

	CONFIG.debug.modItemHints = false;

	game.modules.get(module).api = {
		HintClass: Hint,
		addHandler: (callback) => exItemHandlers.push(callback)
	}

	console.log(`ITEM HINTS | ${game.modules.get(module).data.version} | READY`);
});
