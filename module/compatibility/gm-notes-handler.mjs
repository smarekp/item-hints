import { itemHandlers, createHintElement, Hint } from '../common.mjs';

function gmNotesHandler(options, hints) {
	if (!game.user.isGM) return;
	const item = options.item;

	const notes = item.getFlag('gm-notes', 'notes');
	if (notes === undefined) return;

	const gmhint = createHintElement(
		new Hint(game.i18n.localize('GMNote.label'), ['module', 'gm-notes'], { hint: game.i18n.localize('MKAh.Module.GMNotes.NotesFound'), icon: 'fas fa-clipboard-check' }),
		options.iconize
	);
	hints.append(gmhint);
}

{
	itemHandlers.push(gmNotesHandler);
}
