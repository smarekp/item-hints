export const module = 'mkah-pf1-item-hints';

export const itemHandlers = [];
export const exItemHandlers = [];

export class Hint {
	label;
	icon;
	image;
	hint;
	additionalClasses;
	inBuiltTag;

	/**
	 * @param label Display label.
	 * @param {Array<String>} classes Array of HTML classes to add to identify the hint.
	 * @param {String} hint Tooltip text.
	 * @param {String} icon Font awesome icon class.
	 * @param {String} image URL to image file.
	 * @param {Boolean} inbuilt Inbuilt tag. Used for weapon properties.
	 */
	constructor(label, classes = [], { hint = null, icon = null, image = null, inbuilt = false } = {}) {
		this.label = label;
		this.hint = hint;
		this.icon = icon;
		this.image = image;
		this.additionalClasses = classes;
		this.inBuiltTag = inbuilt;
	}
}

/**
 * Foundry 0.7.x compatibility mode.
 */
export var foundry07Compatibility = false;
Hooks.once('init', () => {
	foundry07Compatibility = game.data.version.split('.', 3)[1] === '7';
	if (foundry07Compatibility) console.log("ITEM HINTS | Compatibility Mode | Foundry 0.7.x");
});

/**
 * @param {Hint} hint
 * @param {Boolean} iconize Use icons or images where possible.
 * @returns jquery element for the hint
 */
export function createHintElement(hint, iconize = false) {
	if (CONFIG.debug.modItemHints) console.log(hint);

	const isIcon = iconize && (hint.icon || hint.image);
	const type = isIcon ? (hint.image ? 2 : 1) : 0;
	const typeMap = ['span', 'i', 'img'];

	if (hint.icon && !(hint.icon instanceof Array))
		hint.icon = hint.icon?.split(' ');

	const el = document.createElement(typeMap[type]);
	el.classList.add(hint.inBuiltTag ? 'tag' : 'hint');
	switch (type) {
		case 2: // image
			el.src = hint.image;
			break;
		case 1: // icon
			el.classList.add(...hint.icon);
			break;
		default: // 0: span/text
			el.innerHTML = hint.label;
			el.title = hint.hint ?? '';
			break;
	}
	if (isIcon) {
		if (hint.hint ?? hint.label)
			el.title = hint.hint ?? hint.label;
		el.classList.add('icon');
	}
	el.classList.add(...hint.additionalClasses);

	return el;
}
