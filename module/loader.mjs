Hooks.once('ready', () => {
	if (game.modules.get('gm-notes')?.active)
		import('./compatibility/gm-notes-handler.mjs')
			.then(() => console.log("ITEM HINTS | Compatibility | GM Notes"));

});
